package com.duka.androidrad

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class characterSearchFragment : Fragment() {

    private val db = Firebase.firestore


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_character_search, container, false)

        val characterName = view.findViewById<EditText>(R.id.searchNameText)
        val searchButton = view.findViewById<Button>(R.id.searchButton)
        val newCharacterButton = view.findViewById<Button>(R.id.newCharacterButton)

        val searchList = view.findViewById<RecyclerView>(R.id.characterList)


        val editorFragment = CharacterEditorFragment()


        val fragmentTransaction: FragmentTransaction? =
            activity?.supportFragmentManager?.beginTransaction()

        fun refreshRecycler(){
            db.collection("character").get().addOnSuccessListener { result ->
                val characterList = ArrayList<CharacterData>()
                for (data in result.documents) {
                    val character = data.toObject(CharacterData::class.java)
                    if (character != null) {
                        character.ID = data.id
                        characterList.add(character)
                    }

                }
                val recyclerAdapter = CharacterRecyclerAdapter(characterList, fragmentTransaction)
                searchList.adapter = recyclerAdapter

            }
        }

        newCharacterButton?.setOnClickListener{
            editorFragment.arguments = Bundle()
            fragmentTransaction?.replace(R.id.MainFragment, editorFragment)
            fragmentTransaction?.commit()
        }

        searchButton?.setOnClickListener {
            if(characterName.text.isNotEmpty())
                db.collection("character")
                    .whereEqualTo("Name", characterName.text.toString())
                    .get()
                    .addOnSuccessListener {
                        result ->
                        val characterList = ArrayList<CharacterData>()
                        for(data in result.documents)
                        {
                            val character = data.toObject(CharacterData::class.java)
                            if(character != null)
                            {
                                character.ID = data.id
                                characterList.add(character)
                            }

                        }
                        searchList.adapter = CharacterRecyclerAdapter(characterList, fragmentTransaction)
                    }
            else
                refreshRecycler()

        }

        DataChangeTrigger.flagLiveData.observe(this.viewLifecycleOwner, Observer {
            refreshRecycler()
        })


        searchList.layoutManager = LinearLayoutManager(this.context)
        searchList.setHasFixedSize((true))



        refreshRecycler()
        return view
     }



}
package com.duka.androidrad

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

object DataChangeTrigger : ViewModel() {
    val flagLiveData = MutableLiveData<Boolean>()
    var Flag = false
    public fun dataTrigger()
    {
        Flag = !Flag
        flagLiveData.value = Flag
    }
}
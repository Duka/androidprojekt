package com.duka.androidrad

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface FeaturesEndpoints {
    @GET("features")
    fun getFeatures() : Call<ApiResponse>
}

interface ProfEndpoints {
    @GET("proficiencies")
    fun getProficiencies() : Call<ApiResponse>
}
package com.duka.androidrad

import android.app.FragmentManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class CharacterRecyclerAdapter(val items: ArrayList<CharacterData>, val fragmentTransaction : FragmentTransaction?) : RecyclerView.Adapter<CharacterRecyclerAdapter.myViewHolder>() {


    private val db = Firebase.firestore

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.character_recycler_view, parent, false)
        return myViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: myViewHolder, position: Int) {

        fun setClassImage(Class : String)
        {
            if(Class == "Bard")
                holder.classImage.setImageResource(R.drawable.bard)
            else if(Class == "Warlock")
                holder.classImage.setImageResource(R.drawable.warlock)
            else if(Class == "Monk")
                holder.classImage.setImageResource(R.drawable.monk)
            else if(Class == "Ranger")
                holder.classImage.setImageResource(R.drawable.ranger)
            else if(Class == "Sorcerer")
                holder.classImage.setImageResource(R.drawable.sorcerer)
            else if(Class == "Barbarian")
                holder.classImage.setImageResource(R.drawable.barbarian)
        }

        val currentItem = items[position]
        holder.characterName.setText(currentItem.Name)
        holder.characterClass.setText(currentItem.Class)
        holder.characterLevel.setText(currentItem.Level.toString())

        setClassImage(currentItem.Class.toString())

        holder.editButton.setOnClickListener {

            val bundleToSend = Bundle()
            bundleToSend.putString("ID", currentItem.ID)

            holder.editorFragment.arguments = bundleToSend
            fragmentTransaction?.replace(R.id.MainFragment, holder.editorFragment)
            fragmentTransaction?.commit()
        }

        holder.deleteButton.setOnClickListener {
            if(holder.characterName.text.toString() != "Stevo")
                db.collection("character").document(currentItem.ID.toString())
                    .delete()
                    .addOnSuccessListener {
                        Log.d("tag", "Item deleted from Firestore")
                        DataChangeTrigger.dataTrigger()
                    }
                    .addOnFailureListener { Log.w("tag", "not deleted rip") }

        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    class myViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val characterName = itemView.findViewById<TextView>(R.id.recyclerName)
        val characterClass = itemView.findViewById<TextView>(R.id.recyclerClass)
        val characterLevel = itemView.findViewById<TextView>(R.id.recyclerLevel)

        val classImage = itemView.findViewById<ImageView>(R.id.recyclerImage)

        val ID : String?= "null"

        val editorFragment = CharacterEditorFragment()
        val editButton = itemView.findViewById<Button>(R.id.recyclerEdit)
        val deleteButton = itemView.findViewById<Button>(R.id.recyclerDelete)
    }

}
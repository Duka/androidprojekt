package com.duka.androidrad

import com.google.gson.annotations.SerializedName

data class ApiResponse(
    val count : Int,
    val results : List<ResponseData>
)

data class ResponseData(
    val index: String,
    val name: String,
    val url: String
)
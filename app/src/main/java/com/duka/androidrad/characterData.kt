package com.duka.androidrad

data class CharacterData(
    var Name : String?= "steve",
    var Class : String?= "warlock",
    var Level : Int?= 1,

    var Image : String?= "Null",
    var MaxHP : Int?= 0,

    var PassivePerception : Int?= 12,
    var Proficiencies : String?= "yes",
    var Equipment : String?= "sword",

    var Features : String?= "Null",

    var Str : Int?= 8,
    var Dex : Int?= 8,
    var Con : Int?= 8,
    var Int : Int?= 8,
    var Wis : Int?= 8,
    var Cha : Int?= 8,

    var ID : String?= "null"
)
package com.duka.androidrad

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentTransaction
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.random.Random

class CharacterEditorFragment : Fragment() {

    private val db = Firebase.firestore
    private val apiUrl : String = "https://www.dnd5eapi.co/api/"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_character_editor, container, false)

        val characterID = arguments?.getString("ID")

        val CharacterName = view.findViewById<EditText>(R.id.editTextTextPersonName)
        val CharacterLevel = view.findViewById<EditText>(R.id.inputLevel)
        val CharacterHP = view.findViewById<TextView>(R.id.maxHpEdit)

        val CharacterStr = view.findViewById<EditText>(R.id.inputSTR)
        val CharacterDex = view.findViewById<EditText>(R.id.inputDEX)
        val CharacterCon = view.findViewById<EditText>(R.id.inputCON)
        val CharacterInt = view.findViewById<EditText>(R.id.inputINT)
        val CharacterWis = view.findViewById<EditText>(R.id.inputWIS)
        val CharacterCha = view.findViewById<EditText>(R.id.inputCHA)

        val PassivePerception = view.findViewById<TextView>(R.id.passivePerceptionText)
        val Prof = view.findViewById<TextView>(R.id.proficienciesText)
        val Equipment = view.findViewById<EditText>(R.id.EditEquipment)
        val Feats = view.findViewById<TextView>(R.id.featsText)

        val buttonD20 = view.findViewById<Button>(R.id.rollButton)
        val rollResult = view.findViewById<TextView>(R.id.resultText)

        val classImage = view.findViewById<ImageView>(R.id.characterImage)

        val ClassSpinner = view.findViewById<Spinner>(R.id.classSpinner)
        val choices = arrayOf("Warlock", "Bard", "Barbarian", "Monk", "Ranger", "Sorcerer")
        val adapter = ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_dropdown_item, choices)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        ClassSpinner.adapter = adapter

        fun setClassImage(Class : String)
        {
            if(Class == "Bard")
                classImage.setImageResource(R.drawable.bard)
            else if(Class == "Warlock")
                classImage.setImageResource(R.drawable.warlock)
            else if(Class == "Monk")
                classImage.setImageResource(R.drawable.monk)
            else if(Class == "Ranger")
                classImage.setImageResource(R.drawable.ranger)
            else if(Class == "Sorcerer")
                classImage.setImageResource(R.drawable.sorcerer)
            else if(Class == "Barbarian")
                classImage.setImageResource(R.drawable.barbarian)
        }
        fun getProficiencies()
        {
            val baseUrl = apiUrl
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build()
                .create(ProfEndpoints::class.java)

            val retrofitData = retrofitBuilder.getProficiencies()

            retrofitData.enqueue(object : Callback<ApiResponse?> {
                override fun onResponse(
                    call: Call<ApiResponse?>,
                    response: Response<ApiResponse?>
                ) {
                    val stringBuilder = StringBuilder()
                    stringBuilder.append("Proficiencies:\n")

                    val responseList = response.body()!!.results
                    val count = response.body()!!.count

                    for(i in 0 until 4)
                    {
                        stringBuilder.append(responseList.get(i).name)
                        stringBuilder.append("\n")
                    }
                    Prof.setText(stringBuilder.toString())
                }

                override fun onFailure(call: Call<ApiResponse?>, t: Throwable) {
                    Prof.setText("Failed")
                }
            })
        }
        fun getFeatures(Class : String)
        {
            val baseUrl = apiUrl + "classes/" + ClassSpinner.selectedItem.toString().lowercase() + "/"
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl)
                .build()
                .create(FeaturesEndpoints::class.java)

            val retrofitData = retrofitBuilder.getFeatures()

            retrofitData.enqueue(object : Callback<ApiResponse?> {
                override fun onResponse(
                    call: Call<ApiResponse?>,
                    response: Response<ApiResponse?>
                ) {
                    val stringBuilder = StringBuilder()
                    stringBuilder.append("Features:\n")

                    val responseList = response.body()!!.results

                    for(i in 0 until 4)
                    {
                        stringBuilder.append(responseList.get(i).name)
                        stringBuilder.append("\n")
                    }
                    Feats.setText(stringBuilder.toString())
                }

                override fun onFailure(call: Call<ApiResponse?>, t: Throwable) {
                    Feats.setText("Failed")
                }
            })
        }
        if(characterID != null){


            db.collection("character").document(characterID).get().addOnSuccessListener { Character->
                if(Character.exists()){
                    val character = Character.toObject(CharacterData::class.java)

                    CharacterName.setText(character?.Name)
                    CharacterLevel.setText(character?.Level.toString())

                    CharacterStr.setText(character?.Str.toString())
                    CharacterDex.setText(character?.Dex.toString())
                    CharacterCon.setText(character?.Con.toString())
                    CharacterInt.setText(character?.Int.toString())
                    CharacterWis.setText(character?.Wis.toString())
                    CharacterCha.setText(character?.Cha.toString())

                    Prof.setText("Proficiencies:\n${character?.Proficiencies}")
                    Equipment.setText(character?.Equipment)

                    for(selection in choices)
                    {
                        if(selection == character?.Class)
                            ClassSpinner.setSelection(choices.indexOf(selection), true)
                    }
                    setClassImage(character?.Class.toString())
                    getFeatures(character?.Class.toString())
                    getProficiencies()
                }
            }
        }

        ClassSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener
        {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                setClassImage(selectedItem)
                getFeatures(selectedItem)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                PassivePerception.setText("Passive Perception: ")
                Prof.setText("Proficiencies: ")
                Equipment.setText("Equipment: ")
                Feats.setText("Features: ")
            }
        }

        val exitButton = view.findViewById<Button>(R.id.exitButton)
        val saveButton = view.findViewById<Button>(R.id.saveButton)
        val searchFragment = characterSearchFragment()

        fun getCharacter() : CharacterData
        {
            val character = CharacterData()

            if(CharacterStr.getText().toString() != "")
                character.Str = CharacterStr.text.toString().toInt()
            else
                character.Str =0

            if(CharacterInt.getText().toString() != "")
                character.Int = CharacterInt.text.toString().toInt()
            else
                character.Int = 0

            if(CharacterDex.getText().toString() != "")
                character.Dex = CharacterDex.text.toString().toInt()
            else
                character.Dex = 0

            if(CharacterCon.getText().toString() != "")
                character.Con = CharacterCon.text.toString().toInt()
            else
                character.Con = 0

            if(CharacterWis.getText().toString() != "")
                character.Wis = CharacterWis.text.toString().toInt()
            else
                character.Wis = 0

            if(CharacterCha.getText().toString() != "")
                character.Cha = CharacterCha.text.toString().toInt()
            else
                character.Cha = 0

            character.Proficiencies = Prof.text.toString()
            character.Features = Feats.text.toString()
            character.Equipment = Equipment.text.toString()

            if(CharacterName.getText().toString() != "")
                character.Name = CharacterName.text.toString()
            else
                character.Name = "Name"

            character.Class = ClassSpinner.selectedItem.toString()

            if(CharacterLevel.getText().toString() != "")
                character.Level = CharacterLevel.text.toString().toInt()
            else
                character.Level = 1

            return character
        }

        fun displayPassivePerception()
        {
            var preception : Int = 0
            if(CharacterWis.getText().toString() != ""){
                preception = 10 + (CharacterWis.text.toString().toInt() - 10) / 2}

            PassivePerception.setText("Passive Perception: ${preception}")
        }

        fun goToSearch()
        {
            searchFragment.arguments = Bundle()
            val fragmentTransaction: FragmentTransaction? =
                activity?.supportFragmentManager?.beginTransaction()
            fragmentTransaction?.replace(R.id.MainFragment, searchFragment)
            fragmentTransaction?.commit()
        }


        fun saveCharacter(){

            val character = getCharacter()

            val characterMap = hashMapOf(
                "Name" to character.Name,
                "Class" to character.Class,
                "Level" to character.Level,

                "Equipment" to character.Equipment,
                "Features" to character.Features,
                "Proficiencies" to character.Proficiencies,

                "Cha" to character.Cha,
                "Wis" to character.Wis,
                "Int" to character.Int,
                "Con" to character.Con,
                "Dex" to character.Dex,
                "Str" to character.Str

            )

            if(characterID != null)
            {
                db.collection("character").document(characterID)
                    .set(characterMap)
                    .addOnSuccessListener { Log.d("tag", "DocumentSnapshot successfully written!") }
                    .addOnFailureListener { e -> Log.w("tag", "Error writing document", e) }
            }
            else
            {
                db.collection("character").add(characterMap)
            }

        }
        buttonD20.setOnClickListener {
            val random = Random.nextInt(from = 1, until = 21)
            rollResult.setText(random.toString())
        }
        exitButton.setOnClickListener {
            goToSearch()
        }

        saveButton.setOnClickListener {
            saveCharacter()
            goToSearch()
        }

        CharacterWis.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                displayPassivePerception()
            }
        })

        CharacterLevel.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {

                if(CharacterLevel.text.isEmpty()){
                    CharacterHP.setText("0 HP")
                }
                else
                    CharacterHP.setText((CharacterLevel.text.toString().toInt() * 8).toString() + " HP")

            }
        })
        if(characterID == null)
            getProficiencies()
        return view
    }

}